<?php
require_once("conn.php");

if(isset($_GET['userId'])) {
    $userId = $_GET['userId'];

    try {
        $sql = $db->prepare("SELECT id, nome, email, idade, cpf FROM cadastro WHERE id = ?");
        $sql->execute([$userId]);
        $usuario = $sql->fetch(PDO::FETCH_ASSOC);

        // Retorne os detalhes do usuário como JSON
        echo json_encode($usuario);
    } catch (PDOException $e) {
        echo json_encode(['error' => 'Erro ao buscar usuário: ' . $e->getMessage()]);
    }
}
