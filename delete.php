<?php
require_once("conn.php");

$response = array('success' => false, 'message' => '');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $userId = intval($_POST['userId']);

    try {
        $sql = $db->prepare("DELETE FROM cadastro WHERE id = :id");
        $sql->bindParam(':id', $userId, PDO::PARAM_INT);
        if ($sql->execute()) {
            $response['success'] = true;
        } else {
            $response['message'] = 'Erro ao excluir usuário.';
        }
    } catch (PDOException $e) {
        $response['message'] = 'Erro ao excluir usuário: ' . $e->getMessage();
    }
}

echo json_encode($response);
