<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD Dinâmico</title>
    <link rel="stylesheet" href="style.css">
    <script src="jquery/jquery-3.7.1.js"></script>
    <script src="script.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/2242fd39da.js" crossorigin="anonymous"></script>
</head>

<body> 

<!-- formulário -->
<div class="formcorp">
<form id="forms" method="post" autocomplete="off">
  <div class="mb-3">
    <label for="username" class="form-label">Nome</label>
    <input type="text" class="form-control" id="nome" name="username">
  </div>
  <div class="mb-3">
    <label for="usermail" class="form-label">Email</label>
    <input type="text" class="form-control" id="email" name="usermail">
  </div>
  <div class="mb-3">
    <label for="userage" class="form-label">Idade</label>
    <input type="text" class="form-control" id="idade" name="userage">
  </div>
  <div class="mb-3">
    <label for="usercpf" class="form-label">Cpf</label>
    <input type="text" class="form-control" id="cpf" name="usercpf">
  </div>
  <button type="button" id="submit-btn" class="btn btn-primary" onclick="PreencherForms()">Submit</button>
</form>
</div>

<div class="tableform">
<table class="table" id="tabela">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nome</th>
      <th scope="col">E-mail</th>
      <th scope="col">Idade</th>
      <th scope="col">CPF</th>
    </tr>
  </thead>
  
  <tbody>
  </tbody>

</table>

<a class="btn btn-primary" href="search.php" role="button" target="_blank">Buscar</a>

</div>

<script>
  $(document).ready(function(){
    BuscarSetePrimeiros();
  });
</script>

</body>

</html>