<?php
require_once("conn.php");

try{

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sql = $db->prepare("INSERT INTO cadastro (nome, email, idade, cpf) VALUES(?, ?, ?, ?)");
        $success = $sql->execute(array($_POST['username'], $_POST['usermail'], $_POST['userage'], $_POST['usercpf']));

        if ($success){
            echo 'Data Inserted';
        }else{
            echo 'Data Insert Error';
        }
    }
}catch(PDOException $e){
    echo "Insert Connection Failed: " . $e->getMessage();
}
