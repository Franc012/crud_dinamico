<?php
require_once("conn.php");

try{

    $sql = $db->prepare("SELECT nome, email, idade, cpf FROM cadastro ORDER BY id DESC LIMIT 7");
    $sql->execute();
    $resultado = $sql ->fetchAll(PDO::FETCH_ASSOC);

    header('Content-Type: application/json');
    echo json_encode($resultado);
    
}catch(PDOException $e){
    echo "Select Seventh Connection Failed: " . $e->getMessage();
}
