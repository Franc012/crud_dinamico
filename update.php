<?php
require_once("conn.php");

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['Username'];
    $usermail = $_POST['Usermail'];
    $userage = $_POST['Userage'];
    $usercpf = $_POST['Usercpf'];
    $userId = $_POST['userId'];

    try {
        $sql = $db->prepare("UPDATE cadastro SET nome = ?, email = ?, idade = ?, cpf = ? WHERE id = ?");
        $sql->execute([$username, $usermail, $userage, $usercpf, $userId]);

        echo json_encode(['success' => true]);
    } catch (PDOException $e) {
        echo json_encode(['success' => false, 'message' => 'Erro ao editar usuário: ' . $e->getMessage()]);
    }
}
