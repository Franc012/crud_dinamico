<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Busca</title>
    <link rel="stylesheet" href="style.css">
    <script src="jquery/jquery-3.7.1.js"></script>
    <script src="script.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/2242fd39da.js" crossorigin="anonymous"></script>
</head>
<body>

<!-- navbar de pesquisa -->
<nav class="navbar bg-body-tertiary">
  <div class="container-fluid">
    <form class="d-flex" role="search">
      <input class="form-control me-2" id="searchbar" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
  </div>
</nav>

<br><br>

<br><br>

<br><br>

<div class="container-table">
<!-- tabela com todos os elementos -->
  <div class="elementos">
    <table class="table" id="tabela">
      <thead>
        <tr>
          <th scope="col">Id</th>
          <th scope="col">Nome</th>
          <th scope="col">E-mail</th>
          <th scope="col">Idade</th>
          <th scope="col">CPF</th>
        </tr>
      </thead>
      
      <tbody>
        <?php

          require_once("conn.php");

          try {

          $sql = $db->prepare("SELECT id, nome, email, idade, cpf FROM cadastro");
          $sql->execute();
          $resultado = $sql ->fetchAll(PDO::FETCH_ASSOC);

          } catch (PDOException $e) {
              echo "Select All Data Connection Failed: " . $e->getMessage();
          }

          $lines = '';
          $counter = 1;

          foreach ($resultado as $row) {
              
              $id = $row['id'];

              $lines .= '<tr>';
              $lines .= '<td>' . $counter++ . '</td>';
              $lines .= '<td>' . $row['nome'] . '</td>';
              $lines .= '<td>' . $row['email'] . '</td>';
              $lines .= '<td>' . $row['idade'] . '</td>';
              $lines .= '<td>' . $row['cpf'] . '</td>';
              $lines .= '<td>' . '<button type="button" class="btn btn-primary btn-sm editButton" onclick="CriarModalEdicao(' . $id . ')"><i class="fa-solid fa-pen-to-square fa-lg"></i></button>' . '<td>';
              $lines .= '<td>' . '<button type="button" class="btn btn-danger btn-sm" onclick="ShowDeleteModal(' . $id . ')"><i class="fa-solid fa-trash-can"></i></button>' .'</td>';
              
          }

          echo $lines;

        ?>
      </tbody>

    </table>
  </div>
</div>

<!-- modal de update -->
<div class="container-edit">

  <div class="modal fade" id="Mymodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="exampleModalLabel"></h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        
      <div class="modal-body">
        <form id="editForm">
          <input type="hidden" id="userId_edit" name="userId" value="1">
          <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">Nome</span>
            <input type="text" id="username_edit" name="Username" class="form-control" placeholder="" aria-label="Username" aria-describedby="basic-addon1">
          </div>

          <div class="input-group mb-3">
            <span class="input-group-text">E-mail</span>
            <input type="text" id="usermail_edit" name="Usermail" class="form-control" aria-label="Amount (to the nearest dollar)">
          </div>

          <div class="input-group mb-3">
            <span class="input-group-text">Idade</span>
            <input type="text" id="userage_edit" name="Userage" class="form-control" placeholder="" aria-label="">
            
            <span class="input-group-text">CPF</span>
            <input type="text" id="usercpf_edit" name="Usercpf" class="form-control" placeholder="" aria-label="">
          </div>
        </form>
      </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="SalvarAlteracoes()">Salvar Alterações</button>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- modal de exclusao -->
<div class="modal fade" id="deleteModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Tem certeza que deseja Excluir?</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        O Registro será apagado permanentemente.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Voltar</button>
        <button type="button" id="DeleteButton" class="btn btn-danger">Excluir</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>