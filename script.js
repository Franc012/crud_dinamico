
// funcao que envia dados para o DB via Ajax
function InserirDados() {

    var nome = $('#nome').val();
    var email = $('#email').val();
    var idade = $('#idade').val();
    var cpf = $('#cpf').val();

    console.log('Nome:', nome);
    console.log('Email:', email);
    console.log('Idade:', idade);
    console.log('CPF:', cpf);

    $.ajax({
        url:'insert.php',
        method:'POST',
        data: {username: nome, usermail: email, userage: idade, usercpf: cpf},
        dataType:'json',
        success: function(response){
            console.log('Requisição Ok: ', response)
        },
        error: function (response) {
            console.log(response)
         }
    });
}

// função para preenchimento de formulário
function PreencherForms() {
    InserirDados();
    $('#nome').val("");
    $('#email').val("");
    $('#idade').val("");
    $('#cpf').val("");
}

// funcao que retorna sete primeiros dados do DB via Ajax e processa dados em linhas de tabela
function BuscarSetePrimeiros() {
    setInterval(function(){
    $.ajax({
        url: 'seventh.php',
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#tabela tbody').empty();
            var count = 0;
            data.forEach(function(item) {
                var newRow =
                    '<tr>' +
                    '<td>' + (++count) + '</td>' +
                    '<td>' + item.nome + '</td>' +
                    '<td>' + item.email + '</td>' +
                    '<td>' + item.idade + '</td>' +
                    '<td>' + item.cpf + '</td>' +
                    '</tr>';
                $('#tabela tbody').append(newRow);
            });
        },
        error: function(response) {
            console.log(response)
        }
    });

    }, 5000);
}

// cria modal de edição, com dados do usuário preenchido no campo
function CriarModalEdicao(userId) {
    $.ajax({
        url: 'modalData.php',
        method: 'GET',
        data: { userId: userId },
        dataType: 'json',
        success: function(data) {

            $('#userId_edit').val(data.id);
            $('#username_edit').val(data.nome);
            $('#usermail_edit').val(data.email);
            $('#userage_edit').val(data.idade);
            $('#usercpf_edit').val(data.cpf);
        
            $('#Mymodal').modal('show');
        },
        error: function(response) {
            console.log('User Data Search Error:', response);
        }
    });
}

function SalvarAlteracoes() {
    var formData = $('#editForm').serialize(); // Serializa os dados do formulário
    $.ajax({
        url: 'update.php',
        method: 'POST',
        data: formData,
        dataType: 'json',
        success: function(response) {
            if(response.success) {
                $('#Mymodal').modal('hide'); 
                location.reload(); 
            } else {
                alert('Erro ao editar usuário: ' + response.message);
            }
        },
        error: function(response) {
            console.log('User Data Update Error', response);
        }
    });
}

function ShowDeleteModal(userId) {
    $('#deleteModal').modal('show');
    $('#DeleteButton').off('click').on('click', function() { //apaga eventos de clique anteriores e adiciona um novo.
        $.ajax({
            url: 'delete.php',
            method: 'POST',
            data: { userId: userId },
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    location.reload();
                } else {
                    alert('Erro ao excluir usuário: ' + response.message);
                }
            },
            error: function(response) {
                console.log('User Data Deletion Error', response);
            }
        });
    });
}
