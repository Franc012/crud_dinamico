<?php

$serverName = '127.0.0.1';
$port = '3306';
$dbName = 'dados';
$userName = 'root';
$password = 'root';

try{

    $db = new PDO("mysql:host=$serverName;dbname=$dbName;port=$port;", $userName, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}catch(PDOException $e){
    echo "Primary Connection Failed: " . $e->getMessage();
}
